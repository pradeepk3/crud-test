-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2015 at 04:10 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE IF NOT EXISTS `form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(11) NOT NULL,
  `age` int(11) NOT NULL,
  `sex` varchar(7) NOT NULL,
  `dob` varchar(10) NOT NULL,
  `interest` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`id`, `fullname`, `age`, `sex`, `dob`, `interest`) VALUES
(1, '0', 23, 'm', '3-1-1992', 'cricket,tennis,batminton'),
(2, '0', 23, 'male', '3-1-1992', 'cricket,tennis,batminton'),
(3, 'pradeep', 23, 'male', '3-3-2003', 'cricket,hocky'),
(4, 'pradeep', 23, 'male', '1-3-2004', 'football,tennis,batminton'),
(7, 'sanaa', 21, 'female', '3-3-1993', 'football,hocky'),
(8, 'sana', 20, 'female', '1-3-1992', 'cricket,tennis,batminton');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
