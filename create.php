<?php 
include('config.php');
if(isset($_POST['submit'])){
	//echo"<pre>";print_r($_POST);echo"</pre>";
	$name = $_POST['name'];
	$age = $_POST['age'];
	$gender  = isset($_POST['gender'])?$_POST['gender']:'';
	$dob = $_POST['dob-date']."-".$_POST['dob-month']."-".$_POST['dob-year'];
	$interest = isset($_POST['interest'])?$_POST['interest']:'';
	$interest = implode(',', $interest);
	
	$qry = "insert into `form` (fullname,age,sex,dob,interest) values('$name','$age','$gender','$dob','$interest')";
	$qry_run = mysqli_query($conn, $qry);
	if($qry_run){
		echo "<div class='alert alert-success'>Inserted succesfully !</div>";
	} else{
		echo "<div class='alert alert-error'>something wrong !</div>";
	}
}

?>
 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>PHP CRUD test - CREATE</title>
 	<link rel="stylesheet" href="css/bootstrap.min.css">
 	<script src="js/bootstrap.min.js" type="text/javascript"></script>
 	<script src="js/jquery.js" type="text/javascript"></script>
 </head>
 <body>
	<section class="container">
		<h2>PHP CRUD testing</h2>
		<hr>
		<div class="pull-right"><a href="index.php" class="btn btn-success">Go Back</a></div>
		<br><br><br>
		<table class="table">
			<form action="create.php" method="POST">
				<tr>
					<td>Name</td>
					<td><input type="text" name="name" id="name"></td>
				</tr>
				<tr>
					<td>Age</td>
					<td><input type="text" name="age" id="age"></td>
				</tr>
				<tr>
					<td>Gender</td>
					<td><input type="radio" name="gender" value="male">&nbsp;&nbsp;Male&nbsp;&nbsp;<input type="radio" name="gender" value="female">&nbsp;&nbsp;Female</td>
				</tr>
				<tr>
					<td>DOB</td>
					<td>
						<select name="dob-date" id="dob-date">
							<?php
								for($i=1;$i<=31;$i++){
									echo "<option value=".$i.">".$i."</option>";
								}
							?>
						</select>
						<select name="dob-month" id="dob-month">
							<?php
								for($i=1;$i<=12;$i++){
									echo "<option value=".$i.">".$i."</option>";
								}
							?>
						</select>
						<select name="dob-year" id="dob-year">
							<?php
								for($i=2010;$i>=1960;$i--){
									echo "<option value=".$i.">".$i."</option>";
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Interest</td>
					<td>
						<input type="checkbox" name="interest[]" value="cricket">&nbsp;Cricket&nbsp;
						<input type="checkbox" name="interest[]" value="football">&nbsp;Football&nbsp;
						<input type="checkbox" name="interest[]" value="tennis">&nbsp;Tennis&nbsp;
						<input type="checkbox" name="interest[]" value="batminton">&nbsp;Batminton&nbsp;
						<input type="checkbox" name="interest[]" value="hocky">&nbsp;Hocky&nbsp;
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Create" class="btn btn-success"></td>
				</tr>
			</form>
		</table>
	</section>
 </body>
 </html>