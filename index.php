<?php 
include('config.php');
 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>PHP CRUD test</title>
 	<link rel="stylesheet" href="css/bootstrap.min.css">
 	<script src="js/bootstrap.min.js" type="text/javascript"></script>
 	<script src="js/jquery.js" type="text/javascript"></script>
 </head>
 <body>
	<section class="container">
		<h2>PHP CRUD testing - HOME</h2>
		<hr>
		<div class="pull-right"><a href="create.php" class="btn btn-primary">Create Record</a></div>
		<br><br><br>
		<table class="table table-hover">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Age</th>
				<th>Sex</th>
				<th>DOB</th>
				<th>Interest</th>
				<th>Action</th>
			</tr>
			<?php
			$qry = "SELECT id, fullname, age, sex, dob, interest from form order by id asc";
			$result = mysqli_query($conn, $qry);
			while($row = mysqli_fetch_assoc($result)) {
				echo "<tr>";
				echo "<td>".$row['id']."</td>";
				echo "<td>".$row['fullname']."</td>";
				echo "<td>".$row['age']."</td>";
				echo "<td>".$row['sex']."</td>";
				echo "<td>".$row['dob']."</td>";
				echo "<td>".$row['interest']."</td>";
				echo "<td><a href='edit.php?id=".$row['id']."' class='btn btn-default'>Edit</a>&nbsp;&nbsp;<a onclick='delete_record(".$row['id'].");' class='btn btn-default'>Delete</a></td>";
				echo "<tr>";
			}
			?>
		</table>
	</section>
<script type="text/javascript">
	function delete_record( id ){
 
    var answer = confirm('Are you sure?');
 
    //if user clicked ok
    if ( answer ){
        //redirect to url with action as delete and id to the record to be deleted
        window.location = 'delete.php?id=' + id;
    }
}
</script>
 </body>
 </html>