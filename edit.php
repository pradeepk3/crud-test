<?php 
include('config.php');
$id = $_GET['id'];


if(isset($_POST['submit'])){
	//echo"<pre>";print_r($_POST);echo"</pre>";
	$name = $_POST['name'];
	$age = $_POST['age'];
	$gender  = isset($_POST['gender'])?$_POST['gender']:'';
	$dob = $_POST['dob-date']."-".$_POST['dob-month']."-".$_POST['dob-year'];
	$interest = isset($_POST['interest'])?$_POST['interest']:'';
	$interest = implode(',', $interest);
	
	if(!empty($name) && !empty($age) && !empty($gender) && !empty($dob) && !empty($interest)){
		$qry = "update `form` set fullname='$name',age='$age',sex='$gender',dob='$dob',interest='$interest' where id='$id'";
		$qry_run = mysqli_query($conn, $qry);
		if($qry_run){
			echo "<div class='alert alert-success'>Updated succesfully !</div>";
		} else{
			echo "<div class='alert alert-error'>something wrong !</div>";
		}
	} else{
			echo "<div class='alert alert-error'>Fill all the fields !</div>";
 
	}
}

?>
 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>PHP CRUD test</title>
 	<link rel="stylesheet" href="css/bootstrap.min.css">
 	<script src="js/bootstrap.min.js" type="text/javascript"></script>
 	<script src="js/jquery.js" type="text/javascript"></script>
 </head>
 <body>
	<section class="container">
		<h2>PHP CRUD testing - EDIT</h2>
		<hr>
		<div class="pull-right"><a href="index.php" class="btn btn-success">Go Back</a></div>
		<br><br><br>
		<table class="table">
			<?php
			$sel_qry = "SELECT id, fullname, age, sex, dob, interest from form where id='$id'";
			$sel_res = mysqli_query($conn, $sel_qry);
			while($row = mysqli_fetch_assoc($sel_res)){
				$interest = explode(',', $row['interest']);
				//print_r($interest);
				$dob = explode('-', $row['dob']);
				//print_r($dob);
			?>
			<form action="edit.php?id=<?php echo $id;?>" method="POST">
				<tr>
					<td>Name</td>
					<td><input type="text" name="name" id="name" value="<?php echo $row['fullname']; ?>"></td>
				</tr>
				<tr>
					<td>Age</td>
					<td><input type="text" name="age" id="age" value="<?php echo $row['age']; ?>"></td>
				</tr>
				<tr>
					<td>Gender</td>
					<td><input type="radio" name="gender" value="male" <?php if($row['sex']=='male'){echo "checked";} ?>>&nbsp;&nbsp;Male&nbsp;&nbsp;<input type="radio" name="gender" value="female"<?php if($row['sex']=='female'){echo "checked";} ?>>&nbsp;&nbsp;Female</td>
				</tr>
				<tr>
					<td>DOB</td>
					<td>
						<select name="dob-date" id="dob-date">
							<?php
								for($i=1;$i<=31;$i++){
								?>
									<option value="<?php echo $i;?>" <?php if($dob[0]==$i){echo 'selected';} ?>><?php echo $i;?></option>
								<?php
								}
							?>
						</select>
						<select name="dob-month" id="dob-month">
							<?php
								for($i=1;$i<=12;$i++){
								?>
									<option value="<?php echo $i;?>" <?php if($dob[1]==$i){echo 'selected';} ?>><?php echo $i;?></option>
								<?php
								}
							?>
						</select>
						<select name="dob-year" id="dob-year">
							<?php
								for($i=2010;$i>=1960;$i--){
								?>
									<option value="<?php echo $i;?>" <?php if($dob[2]==$i){echo 'selected';} ?>><?php echo $i;?></option>
								<?php
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Interest</td>
					<td>
						<input type="checkbox" name="interest[]" value="cricket" <?php if(in_array("cricket", $interest)){echo "checked";} ?>>&nbsp;Cricket&nbsp;
						<input type="checkbox" name="interest[]" value="football" <?php if(in_array("football", $interest)){echo "checked";} ?>>&nbsp;Football&nbsp;
						<input type="checkbox" name="interest[]" value="tennis" <?php if(in_array("tennis", $interest)){echo "checked";} ?>>&nbsp;Tennis&nbsp;
						<input type="checkbox" name="interest[]" value="batminton" <?php if(in_array("batminton", $interest)){echo "checked";} ?>>&nbsp;Batminton&nbsp;
						<input type="checkbox" name="interest[]" value="hocky" <?php if(in_array("hocky", $interest)){echo "checked";} ?>>&nbsp;Hocky&nbsp;
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" name="submit" value="Update" class="btn btn-success"></td>
				</tr>
			</form>
			<?php
				}
			?>
		</table>
	</section>
 </body>
 </html>